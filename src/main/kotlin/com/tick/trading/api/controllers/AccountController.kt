package com.tick.trading.api.controllers

import com.tick.trading.api.dto.AccountDto
import com.tick.trading.api.dto.TransferMoneyDto
import com.tick.trading.exceptions.AccountNotFoundException
import com.tick.trading.services.AccountService
import org.springframework.http.MediaType
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping(value = ["/v1/accounts"], produces = [MediaType.APPLICATION_JSON_VALUE])
class AccountController(
    private val accountService: AccountService
) {
    @GetMapping("/{accountId}")
    fun getAccount(@PathVariable("accountId") accountId: Long): AccountDto {
        return accountService.getAccountDetails(accountId) ?: throw AccountNotFoundException("Could not find account $accountId")
    }

    @PostMapping("/transfer")
    fun transferMoney(@RequestBody transferMoneyDto: TransferMoneyDto) {
        accountService.transferMoney(transferMoneyDto)
    }
}
