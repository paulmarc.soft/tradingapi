package com.tick.trading.api.controllers.handlers

import com.tick.trading.exceptions.AccountNotFoundException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus

@ControllerAdvice
class AccountNotFoundHandler {
    @ResponseBody
    @ExceptionHandler(AccountNotFoundException::class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    fun accountNotFoundHandler(ex: AccountNotFoundException) = ex.message
}
