package com.tick.trading.api.controllers.handlers

import com.tick.trading.exceptions.NotEnoughBalanceException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus

@ControllerAdvice
class NotEnoughBalanceExceptionHandler {
    @ResponseBody
    @ExceptionHandler(NotEnoughBalanceException::class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    fun notEnoughBalanceExceptionHandler(ex: NotEnoughBalanceException) = ex.message
}
