package com.tick.trading.api.controllers.handlers

import com.tick.trading.exceptions.SystemException
import org.springframework.http.HttpStatus
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseBody
import org.springframework.web.bind.annotation.ResponseStatus

@ControllerAdvice
class SystemExceptionHandler {
    @ResponseBody
    @ExceptionHandler(SystemException::class)
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    fun systemExceptionHandler(ex: SystemException) = ex.message
}
