package com.tick.trading.api.dto

import java.math.BigDecimal

class AccountDto(
    val accountId: Long?,
    val accountBalance: BigDecimal?
)
