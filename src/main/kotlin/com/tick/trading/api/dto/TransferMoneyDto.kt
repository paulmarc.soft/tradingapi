package com.tick.trading.api.dto

import java.math.BigDecimal
import javax.validation.constraints.DecimalMin

data class TransferMoneyDto(
    val fromAccountId: Long,
    val toAccountId: Long,
    @DecimalMin(value = "0.0", inclusive = false)
    val amount: BigDecimal
)
