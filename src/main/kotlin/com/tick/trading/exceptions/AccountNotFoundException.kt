package com.tick.trading.exceptions

class AccountNotFoundException(message: String) : RuntimeException(message)
