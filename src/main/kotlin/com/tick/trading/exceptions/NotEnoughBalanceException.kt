package com.tick.trading.exceptions

class NotEnoughBalanceException(message: String) : RuntimeException(message)
