package com.tick.trading.exceptions

class SystemException(message: String) : RuntimeException(message)
