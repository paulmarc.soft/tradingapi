package com.tick.trading.repositories

import com.tick.trading.repositories.models.Account
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.data.repository.query.Param
import org.springframework.transaction.annotation.Transactional
import java.math.BigDecimal

@Transactional(readOnly = true)
interface AccountRepository : JpaRepository<Account, Long> {
    @Query(
        value = """
        SELECT balance
        FROM account
        WHERE id = :accountId
        """,
        nativeQuery = true
    )
    fun getBalance(@Param("accountId") accountId: Long): BigDecimal

    @Query(
        value = """
        UPDATE account
        SET balance = balance + :amount
        WHERE id = :accountId
        """,
        nativeQuery = true
    )
    @Modifying
    @Transactional
    fun addBalance(@Param("accountId") accountId: Long, @Param("amount") amount: BigDecimal): Int
}
