package com.tick.trading.repositories.models

import com.tick.trading.api.dto.AccountDto
import java.math.BigDecimal
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Table

@Table(name = "account")
@Entity
data class Account(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    val id: Long,

    @Column(name = "balance", nullable = false)
    val balance: BigDecimal
)

fun Account.toDto(): AccountDto = AccountDto(
    accountId = this.id,
    accountBalance = this.balance
)
