package com.tick.trading.services

import com.tick.trading.api.dto.AccountDto
import com.tick.trading.api.dto.TransferMoneyDto
import com.tick.trading.exceptions.AccountNotFoundException
import com.tick.trading.exceptions.NotEnoughBalanceException
import com.tick.trading.exceptions.SystemException
import com.tick.trading.repositories.AccountRepository
import com.tick.trading.repositories.models.toDto
import org.springframework.data.repository.findByIdOrNull
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Isolation
import org.springframework.transaction.annotation.Transactional

@Service
class AccountService(
    private val accountRepository: AccountRepository
) {
    fun getAccountDetails(accountId: Long): AccountDto = (
        accountRepository.findByIdOrNull(accountId) ?: throw AccountNotFoundException("Could not find account $accountId")
        ).toDto()

    @Transactional(isolation = Isolation.REPEATABLE_READ)
    fun transferMoney(
        transferMoneyDto: TransferMoneyDto
    ) {
        val fromAccount = transferMoneyDto.fromAccountId
        val toAccount = transferMoneyDto.toAccountId
        val amount = transferMoneyDto.amount
        val payerAccount = accountRepository.findByIdOrNull(fromAccount) ?: throw AccountNotFoundException("Could not find account $fromAccount")
        accountRepository.findByIdOrNull(toAccount) ?: throw AccountNotFoundException("Could not find account $toAccount")
        if (payerAccount.balance < amount) {
            throw NotEnoughBalanceException("Couldn't transfer money. Check balance and try again!")
        }
        var status = true
        status = status and (
            accountRepository.addBalance(
                fromAccount,
                amount.negate()
            ) > 0
            )
        status = status and (
            accountRepository.addBalance(
                toAccount,
                amount
            ) > 0
            )
        if (!status) {
            throw SystemException("Money transfer failed!")
        }
    }
}
