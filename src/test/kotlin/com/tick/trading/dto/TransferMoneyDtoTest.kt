package com.tick.trading.dto

import com.tick.trading.api.dto.TransferMoneyDto
import com.tick.trading.helpers.ValidationHelper.assertViolatesPositiveAmount
import com.tick.trading.helpers.ValidationHelper.assertZeroViolations
import com.tick.trading.helpers.ValidationHelper.validate
import org.junit.jupiter.api.Test
import java.math.BigDecimal

internal class TransferMoneyDtoTest {
    @Test
    fun `validation succeeds with valid dto`() {
        validate(
            TransferMoneyDto(
                fromAccountId = 1L,
                toAccountId = 2L,
                amount = BigDecimal.ONE
            )
        ).assertZeroViolations()
    }

//    @Test
//    fun `validation fails when amount is negative`() {
//        validate(
//            TransferMoneyDto(
//                fromAccountId = 1L,
//                toAccountId = 2L,
//                amount = BigDecimal(-1)
//            )
//        ).assertViolatesPositiveAmount("amount")
//    }
}
