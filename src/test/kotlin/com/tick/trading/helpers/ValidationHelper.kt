package com.tick.trading.helpers

import assertk.assertThat
import assertk.assertions.isEmpty
import assertk.assertions.matchesPredicate
import javax.validation.ConstraintViolation
import javax.validation.Validation
import javax.validation.Validator
import javax.validation.constraints.DecimalMin

object ValidationHelper {
    private var validator: Validator = Validation.buildDefaultValidatorFactory().validator

    fun <T> validate(entity: T): Set<ConstraintViolation<T>> =
        validator.validate(entity)

    fun <T> Set<ConstraintViolation<T>>.assertZeroViolations() =
        assertThat(this).isEmpty()

    fun <T, S : Any> assertContainsViolation(
        violations: Set<ConstraintViolation<T>>,
        propertyPath: String,
        message: String,
        annotationClass: Class<S>
    ) {
        assertThat(violations)
            .matchesPredicate { list ->
                list.stream().anyMatch {
                    it.propertyPath.toString() == propertyPath &&
                        it.message == message &&
                        annotationClass.isInstance(it.constraintDescriptor.annotation)
                }
            }
    }

    fun <T> Set<ConstraintViolation<T>>.assertViolatesPositiveAmount(propertyPath: String) =
        assertContainsViolation(
            violations = this,
            propertyPath = propertyPath,
            message = "Amount is negative",
            annotationClass = DecimalMin::class.java
        )
}
