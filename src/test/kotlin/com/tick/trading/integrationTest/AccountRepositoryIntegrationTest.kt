package com.tick.trading.integrationTest

import com.tick.trading.repositories.AccountRepository
import com.tick.trading.repositories.models.Account
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.repository.findByIdOrNull
import java.math.BigDecimal
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AccountRepositoryIntegrationTest {

    @Autowired
    private lateinit var accountRepository: AccountRepository

    @BeforeEach
    fun before() {
        accountRepository.save(Account(1L, BigDecimal(999)))
    }

    @Test
    fun `getBalance should return account balance`() {
        val balance = accountRepository.getBalance(1L)
        assertEquals(0, balance.compareTo(BigDecimal(999)))
    }

    @Test
    fun `addBalance should add the amount to the current balance`() {
        accountRepository.addBalance(1L, BigDecimal(999).negate())
        val savedAccount = accountRepository.findByIdOrNull(1L)
        assertNotNull(savedAccount)
        assertEquals(0, savedAccount.balance?.compareTo(BigDecimal(0)))
    }
}
