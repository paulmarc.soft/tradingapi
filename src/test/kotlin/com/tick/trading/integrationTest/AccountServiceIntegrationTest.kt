package com.tick.trading.integrationTest

import com.tick.trading.api.dto.TransferMoneyDto
import com.tick.trading.repositories.AccountRepository
import com.tick.trading.repositories.models.Account
import com.tick.trading.services.AccountService
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.data.repository.findByIdOrNull
import java.math.BigDecimal
import java.util.concurrent.CountDownLatch
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors
import java.util.concurrent.TimeUnit
import kotlin.test.assertEquals
import kotlin.test.assertNotNull

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class AccountServiceIntegrationTest {

    @Autowired
    private lateinit var accountRepository: AccountRepository

    @Autowired
    private lateinit var accountService: AccountService

    @Test
    fun `transferMoney ignores other concurrent calls`() {
        accountRepository.save(Account(1L, BigDecimal(10)))
        accountRepository.save(Account(2L, BigDecimal(0)))

        val executorService: ExecutorService = Executors.newFixedThreadPool(2)
        executorService.execute(
            Thread {
                accountService.transferMoney(TransferMoneyDto(1L, 2L, BigDecimal(5)))
            }
        )
        executorService.execute(
            Thread {
                accountService.transferMoney(TransferMoneyDto(1L, 2L, BigDecimal(5)))
            }
        )
        executorService.shutdown()
        executorService.awaitTermination(10, TimeUnit.SECONDS)

        val firstAccount = accountRepository.findByIdOrNull(1L)
        assertNotNull(firstAccount)
        assertEquals(0, firstAccount.balance?.compareTo(BigDecimal(5)))

        val secondAccount = accountRepository.findByIdOrNull(2L)
        assertNotNull(secondAccount)
        assertEquals(0, secondAccount.balance?.compareTo(BigDecimal(5)))
    }

    @Test
    @Throws(InterruptedException::class)
    fun `transferMoney should handle parallel calls`() {
        accountRepository.save(Account(1L, BigDecimal(10)))
        accountRepository.save(Account(2L, BigDecimal(0)))

        assertEquals(0, accountRepository.getBalance(1L).compareTo(BigDecimal(10)))
        assertEquals(0, accountRepository.getBalance(2L).compareTo(BigDecimal.ZERO))
        val threadCount = 20
        val startLatch = CountDownLatch(1)
        val endLatch = CountDownLatch(threadCount)
        for (i in 0 until threadCount) {
            Thread {
                try {
                    startLatch.await()
                    accountService.transferMoney(TransferMoneyDto(
                        1L,
                        2L,
                        BigDecimal(5))
                    )
                } catch (e: Exception) {
                } finally {
                    endLatch.countDown()
                }
            }.start()
        }
        startLatch.countDown()
        endLatch.await()
        assertEquals(0, accountRepository.getBalance(1L).compareTo(BigDecimal(0)))
        assertEquals(0, accountRepository.getBalance(2L).compareTo(BigDecimal(10)))
    }
}
