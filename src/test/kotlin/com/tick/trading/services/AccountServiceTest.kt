package com.tick.trading.services

import com.tick.trading.api.dto.TransferMoneyDto
import com.tick.trading.exceptions.AccountNotFoundException
import com.tick.trading.exceptions.NotEnoughBalanceException
import com.tick.trading.exceptions.SystemException
import com.tick.trading.repositories.AccountRepository
import com.tick.trading.repositories.models.Account
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.verify
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import org.mockito.ArgumentMatchers.any
import org.springframework.data.repository.findByIdOrNull
import java.math.BigDecimal
import kotlin.test.assertNotNull

class AccountServiceTest {

    @MockK
    private lateinit var accountRepository: AccountRepository

    private lateinit var accountService: AccountService

    @BeforeEach
    fun setup() {
        MockKAnnotations.init(this)
        accountService = AccountService(accountRepository)
    }

    @Test
    fun `getAccountDetails throws AccountNotFoundException when findByIdOrNull returns null`() {
        every { accountRepository.findByIdOrNull(1L) } returns null
        assertThrows<AccountNotFoundException> { accountService.getAccountDetails(1L) }
        verify(exactly = 1) {
            accountRepository.findByIdOrNull(1L)
        }
    }

    @Test
    fun `getAccountDetails forwards call to repository`() {
        every { accountRepository.findByIdOrNull(1) } returns Account(1, BigDecimal(999))
        val result = accountService.getAccountDetails(1)
        assertNotNull(result)
        assert(result.accountId == 1L)
        assert(result.accountBalance?.compareTo(BigDecimal(999)) == 0)
        verify(exactly = 1) {
            accountRepository.findByIdOrNull(1)
        }
    }

    @Test
    fun `transferMoney updates the balance of both accounts`() {
        every { accountRepository.findByIdOrNull(any()) } returns Account(1, BigDecimal.TEN)
        every { accountRepository.addBalance(any(), any()) } returns 1
        accountService.transferMoney(TransferMoneyDto(1, 2, BigDecimal.ONE))
        verify(exactly = 2) {
            accountRepository.addBalance(any(), any())
        }
    }

    @Test
    fun `transferMoney throws AccountNotFoundException if the payer account doesn't exist`() {
        every { accountRepository.findByIdOrNull(1) } returns null
        every { accountRepository.findByIdOrNull(2) } returns Account(2, BigDecimal.TEN)
        assertThrows<AccountNotFoundException> {
            accountService.transferMoney(TransferMoneyDto(1, 2, BigDecimal.ONE))
        }
    }

    @Test
    fun `transferMoney throws AccountNotFoundException if the payee account doesn't exist`() {
        every { accountRepository.findByIdOrNull(1) } returns Account(1, BigDecimal.TEN)
        every { accountRepository.findByIdOrNull(2) } returns null
        assertThrows<AccountNotFoundException> {
            accountService.transferMoney(TransferMoneyDto(1, 2, BigDecimal.ONE))
        }
    }

    @Test
    fun `transferMoney throws NotEnoughBalanceException if the payer doesn't have enough money to transfer`() {
        every { accountRepository.findByIdOrNull(any()) } returns Account(1, BigDecimal.ONE)
        assertThrows<NotEnoughBalanceException> {
            accountService.transferMoney(TransferMoneyDto(1, 2, BigDecimal.TEN))
        }
    }

    @Test
    fun `transferMoney throws SystemException if updating payer balance fails`() {
        every { accountRepository.findByIdOrNull(any()) } returns Account(1, BigDecimal.TEN)
        every { accountRepository.addBalance(1, BigDecimal.ONE.negate()) } returns -1
        every { accountRepository.addBalance(2, BigDecimal.ONE) } returns 1
        assertThrows<SystemException> {
            accountService.transferMoney(TransferMoneyDto(1, 2, BigDecimal.ONE))
        }
        verify(exactly = 2) {
            accountRepository.addBalance(any(), any())
        }
    }

    @Test
    fun `transferMoney throws SystemException if updating payee balance fails`() {
        every { accountRepository.findByIdOrNull(any()) } returns Account(1, BigDecimal.TEN)
        every { accountRepository.addBalance(1, BigDecimal.ONE.negate()) } returns 1
        every { accountRepository.addBalance(2, BigDecimal.ONE) } returns -1
        assertThrows<SystemException> {
            accountService.transferMoney(TransferMoneyDto(1, 2, BigDecimal.ONE))
        }
        verify(exactly = 2) {
            accountRepository.addBalance(any(), any())
        }
    }
}
